<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;



if (!function_exists('get_file')) {
    /**
     * get_file
     *
     * @param  string $file
     * @param  string $folder | $url
     * @return url
     */
    function get_file($folder, $file)
    {
        $default = 'https://via.placeholder.com/250x250';

        $hasFile = File::exists($folder . '/' . $file);
        if ($hasFile) 
            return asset('uploads/'.$folder . '/' . $file);
        else 
            return $default;
    }
}

if (!function_exists('storeImage')) {
    // image store in storage
    function storeImage($folder, $file)
    {
        $fileName = md5($file . microtime()) . '.' . $file->extension();
        $file->storeAs($folder, $fileName);
        return $fileName;
    }
}

if (!function_exists('deleteExistingImage')) {
    // image delete from storage
    function deleteExistingImage($folder, $file)
    {
        if ($file) {
            if (file_exists(public_path() . '/' . 'uploads/' . $folder . '/' . $file)) {
                unlink(public_path() . '/' . 'uploads/' . $folder . '/' . $file);
            }
        }
    }
}

