<?php

namespace App\DataTables\Product;

use App\Libraries\Encryption;
use App\Modules\Product\Models\Product;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;

class ProductListDataTable extends DataTable
{

    /**
     * Display ajax response.
     *
     * @return JsonResponse
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('price', function ($data) {
                return $data->price ? $data->price.' Tk' : '-';
            })
            ->addColumn('action', function ($data) {
                $actionBtn = '<a href="/products/' . Encryption::encodeId($data->id) . '/edit/" class="btn btn-sm btn-primary AppModal" title="Edit" data-toggle="modal" data-target="#AppModal"><i class="fa fa-edit"></i> Edit</a> ';
                $actionBtn .= '<a href="/products/' . Encryption::encodeId($data->id) . '/delete/" class="btn btn-sm btn-danger action-delete" title="Delete"><i class="fa fa-trash"></i> Delete</a>';
                return $actionBtn;
            })
            ->addColumn('status', function ($data) {
                return ($data->status == 1) ? "<label class='badge badge-success'> Active </label>" : "<label class='badge badge-danger'> Inactive </label>";
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     * @return \Illuminate\Database\Eloquent\Builder
     * @internal param \App\Models\AgentBalanceTransactionHistory $model
     */
    public function query()
    {
        $query = Product::getProductList();
        $data = $query->select([
            'products.*'
        ]);
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'dom' => 'Blfrtip',
                'responsive' => true,
                'autoWidth' => false,
                'paging' => true,
                "pagingType" => "full_numbers",
                'searching' => true,
                'info' => true,
                'searchDelay' => 350,
                "serverSide" => true,
                'order' => [[1, 'asc']],
                'buttons' => ['excel', 'csv', 'print', 'reset', 'reload'],
                'pageLength' => 10,
                'lengthMenu' => [[10, 20, 25, 50, 100, 500, -1], [10, 20, 25, 50, 100, 500, 'All']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'product code' => ['data' => 'code', 'name' => 'code', 'orderable' => true, 'searchable' => true],
            'name' => ['data' => 'name', 'name' => 'name', 'orderable' => true, 'searchable' => true],
            'price' => ['data' => 'price', 'name' => 'price', 'orderable' => true, 'searchable' => true],
            'status' => ['data' => 'status', 'name' => 'status', 'orderable' => true, 'searchable' => true],
            'action' => ['searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'products_list_' . date('Y_m_d_H_i_s');
    }
}
