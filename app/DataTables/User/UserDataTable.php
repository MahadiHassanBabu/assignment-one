<?php

namespace App\DataTables\User;

use App\Libraries\Encryption;
use App\Modules\User\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;


class UserDataTable extends DataTable
{

    /**
     * Display ajax response.
     *
     * @return JsonResponse
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->addColumn('action', function ($data) {
                $actionBtn = '<a href="/admin/users/' . Encryption::encodeId($data->id) . '/edit/" class="btn btn-sm btn-primary AppModal" title="Edit" data-toggle="modal" data-target="#AppModal"><i class="fa fa-edit"></i> Edit</a> ';
                $actionBtn .= '<a href="/admin/users/' . Encryption::encodeId($data->id) . '/delete/" class="btn btn-sm btn-danger action-delete" title="Delete"><i class="fa fa-trash"></i> Delete</a>';

                return $actionBtn;
            })

            ->editColumn('first_name', function(User $user) {
                return $user->full_name ?? '-';
            })

            ->filterColumn('first_name', function ($query, $keyword) {
                return $query->where('first_name','LIKE',["%{$keyword}%"])->orWhere('last_name','LIKE',["%{$keyword}%"]);
            })


            ->filterColumn('userType', function ($query, $keyword) {
                $query->whereHas('userType',function($query) use ($keyword){
                    return $query->where('user_type','LIKE',["%{$keyword}%"]);
                });
            })

            ->editColumn('userType', function(User $user) {
                return $user->userType->type_name ?? '-';
            })

            ->editColumn('mobile', function ($data) {

                return $data->mobile ? $data->mobile : '-';
            })
            ->addColumn('status', function ($data) {
                return $data->status ? "<label class='badge badge-primary'> Active </label>" : "<label class='badge badge-danger'> Inactive </label>";
            })
            ->rawColumns(['status','action'])
            ->make(true);

    }

    /**
     * Get query source of dataTable.
     * @return \Illuminate\Database\Eloquent\Builder
     * @internal param \App\Models\AgentBalanceTransactionHistory $model
     */
    public function query()
    {
        $data = User::getUserList();
        $data->select([
            'users.*'
        ]);
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'dom' => 'Blfrtip',
                'responsive' => true,
                'autoWidth' => false,
                'paging' => true,
                "pagingType" => "full_numbers",
                'searching' => true,
                'info' => true,
                'searchDelay' => 350,
                "serverSide" => true,
                'order' => [[1, 'asc']],
                'buttons' => ['excel', 'csv', 'print', 'reset', 'reload'],
                'pageLength' => 10,
                'lengthMenu' => [[10, 20, 25, 50, 100, 500, -1], [10, 20, 25, 50, 100, 500, 'All']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user role' => ['data' => 'userType', 'name' => 'userType', 'orderable' => true, 'searchable' => true],
            'full name' => ['data' => 'first_name', 'name' => 'first_name', 'orderable' => true, 'searchable' => true],
            'email' => ['data' => 'email', 'name' => 'email', 'orderable' => true, 'searchable' => true],
            'mobile' => ['data' => 'mobile', 'name' => 'mobile', 'orderable' => true, 'searchable' => true],
            'status' => ['data' => 'status', 'name' => 'status', 'orderable' => true, 'searchable' => true],
            'action' => ['searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'user_list_' . date('Y_m_d_H_i_s');
    }
}
