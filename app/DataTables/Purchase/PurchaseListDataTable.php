<?php

namespace App\DataTables\Purchase;

use App\Libraries\Encryption;
use App\Modules\Purchase\Models\Purchase;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;

class PurchaseListDataTable extends DataTable
{

    /**
     * Display ajax response.
     *
     * @return JsonResponse
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('price', function ($data) {
                return $data->price ? $data->price.' Tk' : '-';
            })

            ->editColumn('customer_id', function(Purchase $purchase) {
                return $purchase->customer->full_name ?? '-';
            })

            ->editColumn('quantities', function(Purchase $purchase) {
                return $purchase->quantities->sum('quantity') ?? '-';
            })

            ->editColumn('totalAmount', function(Purchase $purchase) {
                return $purchase->totalAmount->sum('total') ?? '-';
            })

            ->filterColumn('customer_id', function ($query, $keyword) {
                $query->whereHas('customer',function($query) use ($keyword){
                    return $query->where('first_name','LIKE',["%{$keyword}%"])->orWhere('last_name','LIKE',["%{$keyword}%"]);
                });
            })

            ->addColumn('action', function ($data) {
                $actionBtn = '';
                if (($data->approve_status == 0 && auth()->user()->user_type == "1x101")) {
                    $actionBtn .= ' <a href="/purchase/approve/'.Encryption::encodeId($data->id).'" class="btn btn-xs btn-success" title="Approve"><i class="fa fa-check-square"></i> Approve</a>';
                }

                if($data->approve_status == 1){
                    if ($data->customer_id == auth()->user()->id || auth()->user()->user_type == "1x101")
                        $actionBtn .= ' <a href="/purchase/invoice-download/'.Encryption::encodeId($data->id).'" class="btn btn-xs btn-primary" title="Download"><i class="fa fa-file-pdf"></i> Download</a>';
                }

                return $actionBtn;
            })
            ->addColumn('status', function ($data) {
                return ($data->status == 1) ? "<label class='badge badge-success'> Active </label>" : "<label class='badge badge-danger'> Inactive </label>";
            })
            ->editColumn('created_at', function ($data) {
                return ($data->created_at) ? $data->created_at->format('d, M  Y').' at '.$data->created_at->format('H:i:s') : '-';
            })
            ->editColumn('total_amount', function ($data) {
                return ($data->total_amount) ? $data->total_amount.' Tk' : '-';
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    /**
     * Get query source of dataTable.
     * @return \Illuminate\Database\Eloquent\Builder
     * @internal param \App\Models\AgentBalanceTransactionHistory $model
     */
    public function query()
    {
        $query = Purchase::getPurchaseList();
        $data = $query->select([
            'purchases.*'
        ]);
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'dom' => 'Blfrtip',
                'responsive' => true,
                'autoWidth' => false,
                'paging' => true,
                "pagingType" => "full_numbers",
                'searching' => true,
                'info' => true,
                'searchDelay' => 350,
                "serverSide" => true,
                'order' => [[1, 'asc']],
                'buttons' => ['excel', 'csv', 'print', 'reset', 'reload'],
                'pageLength' => 10,
                'lengthMenu' => [[10, 20, 25, 50, 100, 500, -1], [10, 20, 25, 50, 100, 500, 'All']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'purchased at' => ['data' => 'created_at', 'name' => 'purchases.created_at', 'orderable' => true, 'searchable' => true],
            'tracking no' => ['data' => 'tracking_no', 'name' => 'purchases.tracking_no', 'orderable' => true, 'searchable' => true],
            'customer' => ['data' => 'customer_id', 'name' => 'customer_id', 'orderable' => true, 'searchable' => true],
            'total product' => ['data' => 'quantities', 'name' => 'quantities', 'orderable' => true, 'searchable' => false],
            'total amount' => ['data' => 'totalAmount', 'name' => 'totalAmount', 'orderable' => true, 'searchable' => false],
            'status' => ['data' => 'status', 'name' => 'purchases.status', 'orderable' => true, 'searchable' => true],
            'action' => ['searchable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'purchase_list_' . date('Y_m_d_H_i_s');
    }
}
