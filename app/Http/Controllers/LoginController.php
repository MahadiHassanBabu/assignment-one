<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login()
    {
        if(auth()->user() && auth()->user()->user_type == "1x101") return redirect('/dashboard');
        if(auth()->user() && auth()->user()->user_type == "2x202") return redirect('/purchases');
        return view('auth.login');
    }

    public function loginCheck(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required'
        ],
            [
                'email.required' => 'The email address field is required.',
                'password.required' => 'The password field is required.'
            ]);

        if(Auth::attempt(['email'=>$request->get('email'),'password'=>$request->get('password')]))
            if(auth()->user() && auth()->user()->user_type == "1x101") return redirect('/dashboard');
            if(auth()->user() && auth()->user()->user_type == "2x202") return redirect('/purchases');

        return redirect('auth.login')->with('flash_danger','Invalid email or password');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
