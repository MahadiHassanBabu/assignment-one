<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <table cellpadding="10" width="100%">
                    <tr>
                        <td style="text-align: left"><strong>Date : {{ \Carbon\Carbon::parse($purchase->created_at)->format('d-m-Y') }}</strong></td>
                        <td style="text-align: right"><strong>Focus Soft</strong></td>
                    </tr>
                </table>
                <table border="1" cellpadding="10" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name (Code)</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $totalAmount = 0; @endphp
                    @foreach($purchaseDetails as $key => $product)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $product->product_name." Tk" }} ({{ $product->product_code }})</td>
                            <td>{{ $product->quantity }}</td>
                            <td>{{ $product->unit_price." Tk" }}</td>
                            <td>{{ $product->total." Tk" }}</td>
                            @php $totalAmount += $product->total; @endphp
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="4">Total Amount</td>
                        <td>{{ $totalAmount? $totalAmount." Tk" : 0  }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
