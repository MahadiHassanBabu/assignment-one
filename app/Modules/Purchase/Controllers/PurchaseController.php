<?php

namespace App\Modules\Purchase\Controllers;

use App\DataTables\Purchase\PurchaseListDataTable;
use App\Libraries\Encryption;
use App\Modules\Product\Models\Product;
use App\Modules\Purchase\Models\Purchase;
use App\Modules\Purchase\Models\PurchaseDetail;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mpdf\Mpdf;

class PurchaseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PurchaseListDataTable $dataTable)
    {
        return $dataTable->render("Purchase::index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['customers'] = User::where('is_archive', 0)
            ->select(DB::raw("CONCAT(first_name, ' ', last_name) AS name"), "id")
            ->whereNotIn('id',[auth()->user()->id])
            ->pluck("name", "id");

        $data['products'] = Product::where('is_archive', 0)->pluck('name', 'id');
        return view("Purchase::create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required'
        ], [
            'customer_id.required' => 'The customer field is required'
        ]);

        $purchaseProducts = Session::get("purchase.products");

        if (!$purchaseProducts) {
            return redirect()->back()->with('flash_danger','You have never select any products.');
        }

        try {
            DB::beginTransaction();
            $purchase   = new Purchase();
            $purchase->customer_id = $request->input('customer_id');
            $purchase->purchase_date  = $request->input('purchase_date');
            $purchase->status = 1;
            $purchase->save();

            foreach ($purchaseProducts as $key => $product) {
                $purchaseDetails = new PurchaseDetail();
                $purchaseDetails->purchase_id = $purchase->id;
                $purchaseDetails->product_id = $product['id'];
                $purchaseDetails->quantity = $product['quantity'];
                $purchaseDetails->unit_price = $product['unit_price'];
                $purchaseDetails->total = $product['quantity']*$purchaseDetails->unit_price;
                $purchaseDetails->status = 1;
                $purchaseDetails->save();
            }

            /* Generating tracking no*/

            $trackingPrefix = "P" . date("Ymd");
            DB::statement("update purchases, purchases as table2  SET purchases.tracking_no=
            ( select concat('$trackingPrefix', LPAD( IFNULL(MAX(SUBSTR(table2.tracking_no,-6,6) )+1,0),6,'0')) as tracking_no
            from (select * from purchases ) as table2 where table2.id!='$purchase->id' and table2.tracking_no like '$trackingPrefix%')
            where purchases.id='$purchase->id' and table2.id='$purchase->id'");

            Session::forget("purchase.products");
            DB::commit();

            Session::flash('flash_success', 'Products purchased successfully');

            return redirect('/purchases');
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('flash_danger', $e->getMessage());
            return redirect()->back();
        }
    }


    public function purchaseApprove($purchaseId){
        $decodedId = Encryption::decodeId($purchaseId);
        $purchase = Purchase::find($decodedId);
        $purchase->approve_status = 1;
        $purchase->save();
        return redirect('/purchases')->with('flash_success', 'Purchase approved successfully!');
    }

    public function downloadPurchaseInvoice($purchaseId){
        $decodedId = Encryption::decodeId($purchaseId);
        $data['purchase'] = Purchase::leftJoin('users','users.id','=','purchases.customer_id')
            ->where('purchases.id',$decodedId)
            ->first([
                'purchases.*',
                DB::raw("CONCAT(users.first_name,' ', users.last_name) AS full_name"),
                'users.email',
                'users.mobile'
            ]);
        $data['purchaseDetails'] = PurchaseDetail::leftJoin('products','products.id','=','purchase_details.product_id')
            ->where('purchase_details.purchase_id',$decodedId)
            ->get([
                'purchase_details.*',
                'products.name as product_name',
                'products.code as product_code',
            ]);
        $contents = view("Purchase::invoice-pdf",$data)->render();
        $mPDF = new mPDF([
            'utf-8', // mode - default ''
            'A4', // format - A4, for example, default ''
            12, // font size - default 0
            'dejavusans', // default font family
            10, // margin_left
            10, // margin right
            10, // margin top
            15, // margin bottom
            10, // margin header
            9, // margin footer
            'P'
        ]);

        $mPDF->Bookmark('Start of the document');
        $mPDF->useSubstitutions;
        $mPDF->SetProtection(array('print'));
        $mPDF->SetDefaultBodyCSS('color', '#000');
        $mPDF->SetTitle("Product Invoice - Invoice");
        $mPDF->SetSubject("Purchase Invoice");
        $mPDF->SetAuthor('Focus Soft');
        $mPDF->autoScriptToLang = true;
        $mPDF->baseScript = 1;
        $mPDF->autoVietnamese = true;
        $mPDF->autoArabic = true;

        $mPDF->autoLangToFont = true;
        $mPDF->SetDisplayMode('fullwidth');
        $mPDF->setFooter('{PAGENO} / {nb}');
//        $stylesheet = file_get_contents('assets/stylesheets/appviewPDF.css');
        $mPDF->setAutoTopMargin = 'stretch';
        $mPDF->setAutoBottomMargin = 'stretch';
//        $mPDF->WriteHTML($stylesheet, 1);

        $mPDF->WriteHTML($contents, 2);

        $mPDF->defaultfooterfontsize = 10;
        $mPDF->defaultfooterfontstyle = 'B';
        $mPDF->defaultfooterline = 0;

        $mPDF->SetCompression(true);
        $mPDF->Output('ABC' . '.pdf', 'I');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productAutoSuggest(Request $request)
    {
        $activeProducts = Product::where(['status'=> 1,'is_archive'=> 0]);
        $term =  $request->input('term');
        if ($term == 'all'){
            $search_items = $activeProducts->orderBy('name', 'desc')->get();
        }else {
            $search_items = $activeProducts->where(function($join) use ($term){
                $join->where('name', 'like', '%' . $term . '%');
                $join->orWhere('code', 'like', '%' . $term . '%');
            })->orderBy('name', 'desc')->get(['id', 'name', 'code']);
        }

        return response()->json($search_items);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function productAddCart(Request $request){
        try {
            $productId = $request->get('productId');
            $data = Product::where('is_archive', 0)->where('id', $productId) ->first();

            if (!$data) {
                return redirect()->back()->with('flash_danger', "This product is not available");
            }

            $productInfo = array();
            $productInfo['id'] = $data->id;
            $productInfo['name'] = $data->name;
            $productInfo['code'] = $data->code;
            $productInfo['price'] = $data->price;
            $productInfo['quantity'] = 1;
            if (Session::get("purchase.products.$productId")) {
                $productInfo['quantity'] = Session::get("purchase.products.$productId.quantity") + 1;
            }
            $productInfo['unit_price'] = $productInfo['quantity'] * $data->price;
            Session::put("purchase.products.$productId", $productInfo);
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('flash_danger', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function productCartEditDelete(Request $request){
        try {
            $getProductInfo = $request->all();
            $productId = $getProductInfo['product_id'];

            if ($getProductInfo['edit_delete'] == 'edit') {
                $productInfo = array();
                $productInfo['id'] = $getProductInfo['product_id'];
                $productInfo['name'] = Session::get("purchase.products.$productId.name");
                $productInfo['code'] = Session::get("purchase.products.$productId.code");
                $productInfo['price'] = $getProductInfo['price'];
                $productInfo['quantity'] = $getProductInfo['quantity'];

                if ($getProductInfo['quantity'] == 0)
                    $productInfo['quantity'] = Session::get("purchase.products.$productId.quantity");

                if ($getProductInfo['unit_price'] < 1)
                    $productInfo['unit_price'] = Session::get("purchase.products.$productId.unit_price");

                $productInfo['unit_price'] =  $productInfo['price'] * $productInfo['quantity'];

                Session::put("purchase.products.$productId", $productInfo);
            } else {
                Session::forget("purchase.products.$productId");
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Session::flash('flash_danger', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function customerAutoload(Request $request)
    {
        try {
            $customerId = $request->input('customer_id');
            if (!empty($customerId)) {
                $query = User::where(['id' => $customerId, 'is_archive' => false])->first();
            }

            if (!$customerId) {
                throw new Exception('Customer not found!');
            }

            $data['data'] = $query->toArray();
            $data['responseCode'] = 1;
            $data['message'] = 'success';


        } catch (Exception $exception) {
            $data['responseCode'] = 0;
            $data['message'] = $exception->getMessage();
        } finally {
            return response()->json($data);
        }
    }

}
