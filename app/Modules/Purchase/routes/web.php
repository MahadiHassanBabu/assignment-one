<?php

use Illuminate\Support\Facades\Route;

Route::group(['module' => 'Purchase', 'middleware' => ['web', 'auth'], 'namespace' => 'App\Modules\Purchase\Controllers'], function () {

    Route::get('purchases', 'PurchaseController@index')->name('purchases.index');

});

Route::group(['module' => 'Purchase', 'middleware' => ['web', 'auth', 'admin'], 'namespace' => 'App\Modules\Purchase\Controllers'], function () {

    Route::get('purchases/create', 'PurchaseController@create')->name('purchases.create');
    Route::post('purchases/store', 'PurchaseController@store')->name('purchases.store');
    Route::get('purchase/approve/{purchaseId}', 'PurchaseController@purchaseApprove');
    Route::get('purchase/invoice-download/{purchaseId}', 'PurchaseController@downloadPurchaseInvoice');
    Route::get('customer-autoload-by-customer-id', 'PurchaseController@customerAutoload');
    Route::get('product-price-autoload-by-product-id', 'PurchaseController@productPriceAutoLoad');


    Route::post('purchase/product/auto-suggest', 'PurchaseController@productAutoSuggest');
    Route::post('purchase/product/add-cart', 'PurchaseController@productAddCart')->name('purchase.product.add.cart');
    Route::post('purchase/product/cart/edit-delete', 'PurchaseController@productCartEditDelete')->name('purchase.product.cart.editDelete');


});

