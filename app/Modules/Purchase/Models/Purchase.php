<?php

namespace App\Modules\Purchase\Models;

use App\Modules\User\Models\User;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model {

    protected $table = 'purchases';
    protected $fillable = [
        'id',
        'customer_id',
        'tracking_no',
        'approve_status',
        'purchase_date',
        'status',
        'is_archive',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function customer(){
        return $this->belongsTo(User::class);
    }

    public function quantities()
    {
        return $this->hasMany(PurchaseDetail::class);
    }

    public function totalAmount(){
        return $this->hasMany(PurchaseDetail::class);
    }

    public static function getPurchaseList()
    {
        $query =  Purchase::with('customer','quantities','totalAmount')->where('is_archive', 0);

        if(auth()->user()->user_type == '2x202'){
            $query->whereHas('customer',function($query){
                return $query->where(['id' => auth()->user()->id,'user_type' => auth()->user()->user_type]);
            });

        }
        return $query->latest();
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($data) {
            $data->created_by = auth()->user()->id;
            $data->updated_by = auth()->user()->id;
        });

        static::updating(function ($data) {
            $data->updated_by = auth()->user()->id;
        });
    }
}
