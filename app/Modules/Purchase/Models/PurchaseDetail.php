<?php

namespace App\Modules\Purchase\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model {

    protected $table = 'purchase_details';
    protected $fillable = [
        'id',
        'purchase_id',
        'product_id',
        'quantity',
        'unit_price',
        'total',
        'status',
        'is_archive',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static function getPurchaseDetailList()
    {
        return Purchase::where('is_archive', 0)->orderBy('id', 'desc');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($data) {
            $data->created_by = auth()->user()->id;
            $data->updated_by = auth()->user()->id;
        });

        static::updating(function ($data) {
            $data->updated_by = auth()->user()->id;
        });
    }

}
