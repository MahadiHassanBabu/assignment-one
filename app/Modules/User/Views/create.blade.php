@extends('backend.layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <span class="card-title"><i class="fa fa-plus-square"></i> Create User</span>
        </div>
        <!-- /.card-header -->
        {!! Form::open(['route'=>'admin.users.store', 'method'=>'post','enctype'=>'multipart/form-data','id'=>'dataForm']) !!}
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('user_type','User Role : ',['class'=>'required-star']) !!}
                        {!! Form::select('user_type',$userRoles,'',['class'=>$errors->has('user_type')?'form-control is-invalid':'form-control required','placeholder'=>'Select role']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('first_name','First Name : ',['class'=>'required-star']) !!}
                        {!! Form::text('first_name','',['class'=>$errors->has('first_name')?'form-control is-invalid':'form-control required','placeholder'=>'First name']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('last_name','Last Name : ',['class'=>'required-star']) !!}
                        {!! Form::text('last_name','',['class'=>$errors->has('last_name')?'form-control is-invalid':'form-control required','placeholder'=>'Last name']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('email','Email : ',['class'=>'required-star']) !!}
                        {!! Form::email('email','',['class'=>$errors->has('email')?'form-control is-invalid':'form-control required','placeholder'=>'Email address']) !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('mobile','Mobile : ',['class'=>'required-star']) !!}
                        {!! Form::text('mobile','',['class'=>$errors->has('mobile')?'form-control is-invalid':'form-control required','placeholder'=>'Mobile']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('status','Status : ',['class'=>'required-star']) !!}
                        {!! Form::select('status',['1'=>'Active','0'=>'Inactive'],'',['class'=>$errors->has('status')?'form-control is-invalid':'form-control required']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('password','Password: ',['class'=>'required-star']) !!}
                        {!! Form::password('password',['class'=>$errors->has('password')?'form-control is-invalid':'form-control required','placeholder'=>'Password']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('password_confirmation','Confirm Password: ',['class'=>'required-star']) !!}
                        {!! Form::password('password_confirmation',['class'=>$errors->has('password_confirmation')?'form-control is-invalid':'form-control required','placeholder'=>'Confirm password']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{ Form::label('photo', 'Photo :',['class'=>'required-star']) }}
                        <br/>
                        <div>
                            <img class="img border" src="{{ url('assets/backend/img/photo.png') }}" id="photoViewer" height="100" width="120">
                        </div>
                        <label class="btn btn-block btn-secondary btn-sm rounded-0" style="width: 120px; cursor: pointer">
                            <input onchange="changePhoto(this)" type="file" name="photo" style="display: none" required>
                            <i class="fa fa-image"></i> Browse
                        </label>
                        <span id="photo_err" class="text-danger" style="font-size: 16px;"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.users.index') }}" class="btn btn-warning"><i class="fa fa-backward"></i> Back</a>
            <button type="submit" class="btn float-right btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
