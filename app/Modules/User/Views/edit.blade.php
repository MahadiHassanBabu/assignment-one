@extends('backend.layouts.modal')
@section('title') <h5><i class="fa fa-edit"></i> Edit User</h5> @endsection
@section('content')
    {!! Form::open(['route'=>array('admin.users.update',\App\Libraries\Encryption::encodeId($user->id)), 'method'=>'patch','id'=>'dataForm','enctype'=>'multipart/form-data']) !!}
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('first_name','First Name : ',['class'=>'required-star']) !!}
                    {!! Form::text('first_name',$user->first_name,['class'=>$errors->has('first_name')?'form-control is-invalid':'form-control required','placeholder'=>'First name']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('last_name','Last Name : ',['class'=>'required-star']) !!}
                    {!! Form::text('last_name',$user->last_name,['class'=>$errors->has('last_name')?'form-control is-invalid':'form-control required','placeholder'=>'Last name']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('email','Email : ',['class'=>'required-star']) !!}
                    {!! Form::email('email',$user->email,['class'=>$errors->has('email')?'form-control is-invalid':'form-control required','placeholder'=>'Email address']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('mobile','Mobile : ',['class'=>'required-star']) !!}
                    {!! Form::text('mobile',$user->mobile,['class'=>$errors->has('mobile')?'form-control is-invalid':'form-control required','placeholder'=>'Mobile']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('user_type','User Role : ',['class'=>'required-star']) !!}
                    {!! Form::select('user_type',$userRoles,$user->user_type,['class'=>$errors->has('user_type')?'form-control is-invalid':'form-control required','placeholder'=>'Select role']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('status','Status : ',['class'=>'required-star']) !!}
                    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'],$user->status,['class'=>$errors->has('status')?'form-control is-invalid':'form-control required']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {{ Form::label('photo', 'Photo :',['class'=>'required-star']) }}
                    {!! Form::hidden('photo',$user->photo) !!}
                    <br/>
                    <div>
                        <img class="img border" src="{{ (!empty($user->photo)? url('/uploads/profile/'.$user->photo):url('/assets/backend/img/photo.png')) }}" id="photoViewer" id="photoViewer" height="100" width="120">
                    </div>
                    <label class="btn btn-block btn-secondary btn-sm rounded-0" style="width: 120px; cursor: pointer">
                        <input onchange="changePhoto(this)" type="file" name="photo" style="display: none" required>
                        <i class="fa fa-image"></i> Browse
                    </label>
                    <span id="photo_err" class="text-danger" style="font-size: 16px;"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
        <button name="actionBtn" id="actionButton" type="submit" value="submit" class="actionButton btn btn-primary btn-sm float-right"><i class="fa fa-save"></i> Update</button>
    </div>
    {!! Form::close() !!}
@endsection
@section('external-js')
    <script type="text/javascript">
        function changePhoto(input) {
            if (input.files && input.files[0]) {
                $("#photo_err").html('');
                var mime_type = input.files[0].type;
                if (!(mime_type == 'image/jpeg' || mime_type == 'image/jpg' || mime_type == 'image/png')) {
                    $("#photo_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoViewer').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection