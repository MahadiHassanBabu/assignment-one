<?php

namespace App\Modules\User\Controllers;

use App\DataTables\User\UserDataTable;
use App\Http\Controllers\Controller;
use App\Libraries\Encryption;
use App\Modules\User\Models\User;
use App\Modules\User\Models\UserType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render("User::index");
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data['userRoles'] = UserType::where(['status' => 1, 'is_archive' => 0])
            ->pluck('type_name', 'id');
        return view('User::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_type' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'mobile' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
            'status' => 'required'
        ]);


        $user = new User();
        $user->user_type = $request->get('user_type');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->mobile = $request->get('mobile');
        $user->password = Hash::make($request->input('password_confirmation'));
        $user->status = $request->get('status');

        $path = 'uploads/profile/';
        if ($request->hasFile('photo')) {
            $_userPhoto = $request->file('photo');
            $mimeType = $_userPhoto->getClientMimeType();
            if (!in_array($mimeType, ['image/jpg', 'image/jpeg', 'image/png']))
                return redirect()->back()->with('flash_danger', 'Invalid file format. Only JPG, JPEG and PNG are allowed.');
            if (!file_exists($path))
                mkdir($path, 0777, true);

            $userPhoto = trim(sprintf('%s', uniqid('UserProfile_', true))) . '.' . $_userPhoto->getClientOriginalExtension();
            Image::make($_userPhoto->getRealPath())->resize(300, 300)->save($path . '/' . $userPhoto);
            $user->photo = $userPhoto;
        }

        $user->save();
        return redirect(route('admin.users.index'))->with('flash_success', 'New user created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($userId)
    {
        $decodedUserId = Encryption::decodeId($userId);
        $data['user'] = User::find($decodedUserId);
        $data['userRoles'] = UserType::where(['status' => 1, 'is_archive' => 0])
            ->pluck('type_name', 'id');
        return view("User::edit", $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $userId)
    {

        try {
            $decodedUserId = Encryption::decodeId($userId);
            $validation = Validator::make($request->all(), [
                'user_type' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => ['required', 'email', Rule::unique('users')->ignore($decodedUserId)],
                'mobile' => 'required',
                'status' => 'required'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'success' => false,
                    'error' => $validation->errors()
                ]);
            }

            $user = User::find($decodedUserId);
            $user->user_type = $request->get('user_type');
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            $user->mobile = $request->get('mobile');
            $user->status = $request->get('status');

            if ($request->hasFile('photo')) {
                $path = 'uploads/profile/';
                $_userProfilePhoto = $request->file('photo');
                $mimeType = $_userProfilePhoto->getClientMimeType();

                if (!in_array($mimeType, ['image/jpg', 'image/jpeg', 'image/png']))
                    throw new Exception('Only PNG or JPEG or JPG type images are allowed!', 123);

                if (!file_exists($path))
                    mkdir($path, 0777, true);

                $previousExistingPhoto = $path . '/' . $user->photo; // get previous photo from folder
                if (File::exists($previousExistingPhoto)) // unlink or remove previous photo from folder
                    @unlink($previousExistingPhoto);

                $userProfilePhoto = trim(sprintf('%s', uniqid('UserProfile_', true))) . '.' . $_userProfilePhoto->getClientOriginalExtension();
                Image::make($_userProfilePhoto->getRealPath())->resize(300, 300)->save($path . '/' . $userProfilePhoto);
                $user->photo = $userProfilePhoto;
            }

            $user->save();

            return response()->json([
                'success' => true,
                'status' => 'User updated successfully.',
                'link' => route('admin.users.index')
            ]);
        } catch (Exception $e) {
            return response()->json([
                'error' => true,
                'status' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
    }

    public function deleteUser($userId)
    {
        $decodedId = Encryption::decodeId($userId);
        $user = User::find($decodedId);
        $user->is_archive = 1;
        $user->deleted_by = auth()->user()->id;
        $user->deleted_at = Carbon::now();
        $user->save();
        session()->flash('flash_success', 'User deleted successfully.');
    }
}
