<?php

Route::group(['prefix' => 'admin','module' => 'User', 'middleware' => ['web','auth','admin'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    /*
     * User Management Routes
     */
    Route::get('users/{userId}/delete','UserController@deleteUser');
    Route::resource('users', 'UserController', ['names' => [
        'index'     => 'admin.users.index',
        'create'    => 'admin.users.create',
        'edit'      => 'admin.users.edit',
        'store'     => 'admin.users.store',
        'update'    => 'admin.users.update',
        'show'      => 'admin.users.show'
    ]]);

});
