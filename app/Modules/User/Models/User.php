<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    protected $table = 'users';
    protected $fillable = [
        'id',
        'user_type',
        'first_name',
        'last_name',
        'email',
        'mobile',
        'photo',
        'password',
        'status',
        'is_archive',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'remember_token'
    ];


    public function userType(){
        return $this->belongsTo(UserType::class,'user_type');
    }

    public static function getUserList()
    {
        return User::where('is_archive', 0)->latest();
    }


    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public static function boot() {
        parent::boot();
        static::creating(function($user) {
            if(auth()->user()){
                $user->created_by = auth()->user()->id;
                $user->updated_by = auth()->user()->id;
            }
        });

        static::updating(function($user) {
            if(auth()->user())
                $user->updated_by = auth()->user()->id;
        });
    }
}
