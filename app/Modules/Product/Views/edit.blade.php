@extends('backend.layouts.modal')
@section('title') <h5><i class="fa fa-edit"></i> Product edit</h5> @endsection
@section('content')
    {!! Form::open(['route'=>array('products.update',\App\Libraries\Encryption::encodeId($product->id)),'method'=>'patch','id'=>'dataForm']) !!}
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('name','Name : ',['class'=>'required-star']) !!}
                    {!! Form::text('name',$product->name,['class'=>$errors->has('name')?'form-control is-invalid':'form-control required','placeholder'=>'Name']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('price','Price : ',['class'=>'required-star']) !!}
                    {!! Form::number('price',$product->price,['class'=>$errors->has('price')?'form-control is-invalid':'form-control required']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('status','Status : ',['class'=>'required-star']) !!}
                    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'],$product->status,['class'=>$errors->has('status')?'form-control is-invalid':'form-control required']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
        <button type="submit" class="btn float-right btn-primary btn-sm"><i class="fa fa-save"></i> Update</button>
    </div>
    {!! Form::close() !!}
    <br/>
@endsection
