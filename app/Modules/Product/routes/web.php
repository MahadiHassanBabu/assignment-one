<?php

Route::group(['module' => 'Product', 'middleware' => ['web','auth','admin'], 'namespace' => 'App\Modules\Product\Controllers'], function() {

    Route::get('products/{id}/delete','ProductController@delete');
    Route::resource('products', 'ProductController');

});
