<?php

use Illuminate\Support\Facades\Route;

Route::group(['module' => 'Dashboard', 'middleware' => ['web','auth','admin'], 'namespace' => 'App\Modules\Dashboard\Controllers'], function() {

    Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

});
