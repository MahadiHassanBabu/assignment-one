<?php

namespace App\Modules\Dashboard\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Product\Models\Product;
use App\Modules\User\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $data['totalProduct'] = Product::where('is_archive',0)->count();
        $data['totalUsers'] = User::where('is_archive',0)->count();
        return view("Dashboard::index", $data);
    }
}
