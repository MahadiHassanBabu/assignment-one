/***********************
 SWEET ALERT START HERE
 ***********************/
function warnBeforeAction(URL, redirectURL) {
    swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Yes, proceed!",
            cancelButtonText: "No, cancel!",
            cancelButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                setTimeout(function () {
                    $.ajax({
                        type: "GET",
                        url: URL,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function () {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = redirectURL;
                        }
                    })
                }, 1000);

            } else {
                swal("Cancelled", "Action Cancelled :)", "error");
            }
        });
}

/************************
 IMAGE PREVIEW START HERE
 ************************/
function changePhoto(input) {
    if (input.files && input.files[0]) {
        $("#photo_err").html('');
        let mime_type = input.files[0].type;
        if (!(mime_type == 'image/jpeg' || mime_type == 'image/jpg' || mime_type == 'image/png')) {
            $("#photo_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
            return false;
        }
        let reader = new FileReader();
        reader.onload = function (e) {
            $('#photoViewer').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

/**************************
 DYNAMIC MODAL SCRIPT HERE
 **************************/
$(document.body).on('click', '.AppModal', function (e) {
    e.preventDefault();
    $('#ModalContent').html('<div style="text-align:center;"><h3 class="text-primary">Loading Form...</h3></div>');
    $('#ModalContent').load(
        $(this).attr('href'),
        function (response, status, xhr) {
            if (status === 'error') {
                alert('error');
                $('#ModalContent').html('<p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText + '</p>');
            }
            return this;
        }
    );
});

/***********************
 SELECT WITH SEARCH BOX
 ***********************/
$('.customerId').select2({
    width:"100%"
});


/*********************************************
 CUSTOMER ONCHANGE AUTOLOAD SCRIPT START HERE
 ********************************************/
$('.customerId').on('change',function(){
    let customerId = $(this).val();
    let parentHtml = $(this).parent().parent().parent();
    let params = {'customer_id':customerId};

    let route = "/customer-autoload-by-customer-id";
    customerAutoloadByCustomerId(this,route,params,parentHtml);
});

function customerAutoloadByCustomerId(e,route,request,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let customerId = request.customer_id;
    console.log(customerId);
    $.ajax({
        type: "GET",
        url: route,
        data: {
            customer_id: customerId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.mobile').val(response.data.mobile);
                parentHtml.find('.firstName').val(response.data.first_name);
                parentHtml.find('.lastName').val(response.data.last_name);

            }else{
                parentHtml.find('.mobile, .firstName, .lastName').val('');
                parentHtml.find('.customerId').addClass('is-invalid');
            }
            $(self).next().hide();
        }
    });
}


/*********************************************
 PRODUCT PRICE AUTOLOAD SCRIPT START HERE
 ********************************************/
$(document).on('change','.productId',function(){
    let productId = $(this).val();
    console.log(productId)
    let parentHtml = $(this).parent().parent();
    let params = {'product_id':productId};

    let route = "/product-price-autoload-by-product-id";
    productPriceAutoloadByProductId(this,route,params,parentHtml);
})

function productPriceAutoloadByProductId(e,route,request,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let productId = request.product_id;
    console.log(productId);
    $.ajax({
        type: "GET",
        url: route,
        data: {
            product_id: productId
        },
        success: function (response) {
            if (response.responseCode == 1)
                parentHtml.find('.price').val(response.data.price);
            else
                parentHtml.find('.price').val('');

            $(self).next().hide();
        }
    });
}



/********************
 VALIDATION START HERE
 ********************/
$('#dataForm').validate({
    errorPlacement: function () {
        return false;
    }
});

